package beans;

public class UserBranchPosition extends User {
	private String branchName;
	private String positionName;

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public String getBranchName() {
		return branchName;
	}

	public String getPositionName() {
		return positionName;
	}
}