package beans;

import java.util.Date;

public class UserComment {
    private int id;
    private int postId;
    private String name;
    private String body;
    private int userId;
    private Date createdAt;

    public void setId(int id) {
    	this.id = id;
    }

    public void setPostId(int postId) {
    	this.postId = postId;
    }

    public void setName(String name) {
    	this.name = name;
    }

    public void setBody(String body) {
    	this.body = body;
    }

    public void setUserId(int userId) {
    	this.userId = userId;
    }

    public void setCreatedAt(Date createdAt) {
    	this.createdAt = createdAt;
    }

    public int getId() {
    	return id;
    }

    public int getPostId() {
    	return postId;
    }

    public String getName() {
    	return name;
    }

    public String getBody() {
    	return body;
    }

    public int getUserId() {
    	return userId;
    }

	public Date getCreatedAt() {
		return createdAt;
	}

}
