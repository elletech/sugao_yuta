package beans;

import java.util.Date;

public class Comment {
    private int id;
    private String body;
    private int postId;
    private int userId;
    private Date createdAt;
    private Date updatedAt;

    public void setId(int id) {
    	this.id = id;
    }

    public void setBody(String body) {
    	this.body = body;
    }

    public void setPostId(int postId) {
    	this.postId = postId;
    }

    public void setUserId(int userId) {
    	this.userId = userId;
    }

    public void setCreatedAt(Date createdAt) {
    	this.createdAt = createdAt;
    }

    public void setUpdatedAt(Date updatedAt) {
    	this.updatedAt = updatedAt;
    }

    public int getId() {
    	return id;
    }

    public String getBody() {
    	return body;
    }

    public int getPostId() {
    	return postId;
    }

    public int getUserId() {
    	return userId;
    }

	public Date getCreatedAt() {
		return createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}
}
