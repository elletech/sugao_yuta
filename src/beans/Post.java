package beans;

import java.io.Serializable;
import java.util.Date;

public class Post implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String subject;
	private String body;
	private String category;
	private int userId;
	private Date createdAt;
	private Date updatedAt;

    public void setId(int id) {
    	this.id = id;
    }

    public void setSubject(String subject) {
    	this.subject = subject;
    }

    public void setBody(String body) {
    	this.body = body;
    }

    public void setCategory(String category) {
    	this.category = category;
    }

    public void setUserId(int userId) {
    	this.userId = userId;
    }

    public void setCreatedAt(Date createdAt) {
    	this.createdAt = createdAt;
    }

    public void setUpdatedAt(Date updatedAt) {
    	this.updatedAt = updatedAt;
    }

    public int getId() {
    	return id;
    }

    public String getSubject() {
    	return subject;
    }

    public String getBody() {
    	return body;
    }

    public String getCategory() {
    	return category;
    }

    public int getUserId() {
    	return userId;
    }

	public Date getCreatedAt() {
		return createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}
}
