package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserBranchPosition;
import exception.SQLRuntimeException;

public class UserBranchPositionDao {

	public List<UserBranchPosition> getUsers(Connection connection, int num) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.login_id as login_id,");
			sql.append("users.password as password,");
			sql.append("users.name as name, ");
			sql.append("users.branch_id as branch_id, ");
			sql.append("users.position_id as position_id, ");
			sql.append("users.is_deleted as is_deleted, ");
			sql.append("users.created_at as created_at, ");
			sql.append("users.updated_at as updated_at, ");
			sql.append("branches.name as branch_name, ");
			sql.append("positions.name as position_name ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN positions ");
			sql.append("ON users.position_id = positions.id ");
			sql.append("ORDER BY created_at DESC limit " + num);
			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<UserBranchPosition> userList = toUserList(rs);

			return userList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserBranchPosition> toUserList(ResultSet rs) throws SQLException {
		List<UserBranchPosition> ret = new ArrayList<UserBranchPosition>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchId = rs.getInt("branch_id");
				int positionId = rs.getInt("position_id");
				String branchName = rs.getString("branch_name");
				String positionName = rs.getString("position_name");
				int isDeleted = rs.getInt("is_deleted");
				Timestamp createdAt = rs.getTimestamp("created_at");
				Timestamp updatedAt = rs.getTimestamp("updated_at");

				UserBranchPosition user = new UserBranchPosition();
				user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setName(name);
				user.setBranchId(branchId);
				user.setPositionId(positionId);
				user.setBranchName(branchName);
				user.setPositionName(positionName);
				user.setIsDeleted(isDeleted);
				user.setCreatedAt(createdAt);
				user.setUpdatedAt(updatedAt);

				ret.add(user);

			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
