package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.PostUser;
import exception.SQLRuntimeException;

public class PostUserDao {
	public List<PostUser> getPostUser(Connection connection, String date1, String date2, String category) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("posts.id as id, ");
			sql.append("posts.subject as subject,");
			sql.append("posts.body as body, ");
			sql.append("posts.category as category, ");
			sql.append("posts.user_id as user_id, ");
			sql.append("users.name as user_name, ");
			sql.append("posts.created_at as created_at ");
			sql.append("FROM posts ");
			sql.append("INNER JOIN users ");
			sql.append("ON posts.user_id = users.id ");
			sql.append("WHERE( posts.created_at BETWEEN ? and ?) ");
			if (category != null && !(category.isEmpty())) {
				sql.append("AND posts.category LIKE ? ");
			}
			sql.append("ORDER BY created_at DESC ");
			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, date1);
			ps.setString(2, date2);
			if (category != null && !(category.isEmpty())) {
				ps.setString(3,  "%"+category+"%");
			}
			ResultSet rs = ps.executeQuery();

			List<PostUser> ret = toPostUserList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<PostUser> toPostUserList(ResultSet rs)
			throws SQLException {

		List<PostUser> ret = new ArrayList<PostUser>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String subject = rs.getString("subject");
				String body = rs.getString("body");
				String category = rs.getString("category");
				int userId = rs.getInt("user_id");
				String userName = rs.getString("user_name");
				Timestamp createdAt = rs.getTimestamp("created_at");

				PostUser postUser = new PostUser();
				postUser.setId(id);
				postUser.setSubject(subject);
				postUser.setBody(body);
				postUser.setCategory(category);
				postUser.setUserId(userId);
				postUser.setUserName(userName);
				postUser.setCreatedAt(createdAt);

				ret.add(postUser);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
