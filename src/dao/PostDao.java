package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Post;
import exception.SQLRuntimeException;

public class PostDao {
    public void insert(Connection connection, Post post) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO posts ( ");
            sql.append("subject");
            sql.append(", body");
            sql.append(", category");
            sql.append(", user_id");
            sql.append(", created_at");
            sql.append(", updated_at");
            sql.append(") VALUES (");
            sql.append(" ?"); // subject
            sql.append(", ?"); // body
            sql.append(", ?"); // category
            sql.append(", ?"); // user_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());
            ps.setString(1, post.getSubject());
            ps.setString(2, post.getBody());
            ps.setString(3, post.getCategory());
            ps.setInt(4, post.getUserId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	public List<Post> getPosts(Connection connection, int num) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM posts ");
			sql.append("ORDER BY created_at DESC limit " + num);
			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<Post> ret = toPostList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Post> toPostList(ResultSet rs) throws SQLException {

		List<Post> ret = new ArrayList<Post>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String subject = rs.getString("subject");
				String body = rs.getString("body");
				String category = rs.getString("category");
				int userId = rs.getInt("user_id");
				Timestamp createdAt = rs.getTimestamp("created_at");
				Timestamp updatedAt = rs.getTimestamp("updated_at");

				Post post = new Post();
				post.setId(id);
				post.setSubject(subject);
				post.setBody(body);
				post.setCategory(category);
				post.setUserId(userId);
				post.setCreatedAt(createdAt);
				post.setUpdatedAt(updatedAt);

				ret.add(post);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void destroy(Connection connection, int postId) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM posts WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, postId);

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
