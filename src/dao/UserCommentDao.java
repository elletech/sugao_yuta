package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {
	public List<UserComment> getUserComments(Connection connection, int num) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id, ");
			sql.append("comments.post_id as post_id,");
			sql.append("comments.body as body, ");
			sql.append("comments.user_id as user_id, ");
			sql.append("users.name as name, ");
			sql.append("comments.created_at as created_at ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY created_at asc limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserComment> ret = toUserCommentList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserComment> toUserCommentList(ResultSet rs)
			throws SQLException {

		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int postId = rs.getInt("post_id");
				String body = rs.getString("body");
				int userId = rs.getInt("user_id");
				String name = rs.getString("name");
				Timestamp createdAt = rs.getTimestamp("created_at");

				UserComment comment = new UserComment();
				comment.setId(id);
				comment.setPostId(postId);
				comment.setBody(body);
				comment.setUserId(userId);
				comment.setName(name);
				comment.setCreatedAt(createdAt);

				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
