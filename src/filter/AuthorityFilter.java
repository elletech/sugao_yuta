package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns = {"/userManager", "/userSetting", "/createUser", "/userRestore", "/userDelete"})
public class AuthorityFilter implements Filter {
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpSession session = ((HttpServletRequest)request).getSession();
        HttpServletResponse httpResponse = (HttpServletResponse)response;

		String path = ((HttpServletRequest) request).getServletPath();//アクセスしたURLのPATHの取得

		//本社総務部でログインしていないときはサーブレットを実行しない
		if(isLogined(session) == false) {
            List<String> messages = new ArrayList<String>();
            messages.add("このページにアクセスする権限がありません。");
            request.setAttribute("errorMessages", messages);
            httpResponse.sendRedirect("./");
            return;
		}
		chain.doFilter(request, response); // サーブレットを実行
	}

	private boolean isLogined(HttpSession session) {

		User user = (User) session.getAttribute("loginUser");
		if (user == null) {
			return false;
		}
		if(user.getBranchId() == 1 && user.getPositionId() == 1) {
			return true;
		}else{
			return false;
		}
	}

	@Override
	public void init(FilterConfig config) {

	}

	@Override
	public void destroy() {
	}

}

