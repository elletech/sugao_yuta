package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	private static final String RESOURCE_PATH = "/login";
	private static final String CSS_PATH = "/css/style.css";
	private static final String LOGIN_CSS_PATH = "/css/login.css";
	private static final String IMG_PATH = "/img";
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpSession session = ((HttpServletRequest)request).getSession();
		String path = ((HttpServletRequest) request).getServletPath();//アクセスしたURLのPATHの取得
		if (path.matches(CSS_PATH)) {
			chain.doFilter(request, response); // サーブレットを実行
			return;
		}
		if (path.matches(IMG_PATH)) {
			chain.doFilter(request, response); // サーブレットを実行
			return;
		}
		//URLがloginならサーブレットを実行
		if(path.toLowerCase().startsWith(RESOURCE_PATH)) {
			chain.doFilter(request, response); // サーブレットを実行
			return;
		}else {
			//URLがloginじゃなくてログインしていないときはサーブレットを実行しない
			if(isLogined(session) == false ) {
				List<String> messages = new ArrayList<String>();
				messages.add("ログインしてください");
				session.setAttribute("errorMessages", messages);
				HttpServletResponse httpResponse = (HttpServletResponse)response;
				httpResponse.sendRedirect("./login");
				return;
			}else{
				chain.doFilter(request, response); // サーブレットを実行
			}
		}
	}

	private boolean isLogined(HttpSession session) {
		User user = (User) session.getAttribute("loginUser");
		if(user != null) {
			return true;
		}else{
			return false;
		}
	}

	@Override
	public void init(FilterConfig config) {

	}

	@Override
	public void destroy() {
	}

}
