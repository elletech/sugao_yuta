package controller;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.PostUser;
import beans.UserComment;
import service.CommentService;
import service.PostService;

@WebServlet(urlPatterns = { "/logout" })
public class LogoutServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        session.invalidate(); // セッションの無効化
        String date1 = "2018-08-01";
        Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date2 = sdf.format(cal.getTime());
        String category = "";
		List<PostUser> posts = new PostService().getPostUser(date1, date2, category);
		List<UserComment> comments = new CommentService().getComment();

		List<String> messages = new ArrayList<String>();
		messages.add("ログアウトしました。");
		response.sendRedirect("./login");
    }
}