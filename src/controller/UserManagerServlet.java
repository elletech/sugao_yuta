package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserBranchPosition;
import service.UserService;

@WebServlet(urlPatterns = { "/userManager" })
public class UserManagerServlet extends HttpServlet  {

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		List<UserBranchPosition> users = new UserService().getUserIndex();
		request.setAttribute("users", users);
		request.getRequestDispatcher("/userManager.jsp").forward(request, response);
	}
}
