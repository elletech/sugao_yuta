package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/newComment" })
public class NewCommentServlet  extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
	}
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		User user = (User) session.getAttribute("loginUser");
		int postId = Integer.parseInt(request.getParameter("postId"));
		Comment comment = new Comment();
		comment.setBody(request.getParameter("commentBody"));
		comment.setPostId(postId);
		comment.setUserId(user.getId());

		if (isValid(request, messages) == true) {
			new CommentService().register(comment);
			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);

			response.sendRedirect("./");
		}
	}
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String comment = request.getParameter("commentBody");

		if (500 < comment.length()) {
			messages.add("コメントは500文字以下で入力してください");
		}
		if (StringUtils.isEmpty(comment) == true) {
			messages.add("コメントを入力してください");
		}
		if (comment.matches("\\s+")) {
			messages.add("コメントを入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
