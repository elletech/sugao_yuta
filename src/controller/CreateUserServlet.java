package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/createUser" })
public class CreateUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		List<Branch> branches = new BranchService().getBranches();
		List<Position> positions = new PositionService().getPositions();
		request.setAttribute("branches", branches);
		request.setAttribute("positions", positions);
		request.getRequestDispatcher("./createUser.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		User user = new User();
		user.setLoginId(request.getParameter("loginId"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setPositionId(Integer.parseInt(request.getParameter("positionId")));

		if (isValid(request, messages) == true) {
			new UserService().register(user);
			response.sendRedirect("./userManager");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("createUser", user);
			List<Branch> branches = new BranchService().getBranches();
			List<Position> positions = new PositionService().getPositions();
			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);
			request.getRequestDispatcher("/createUser.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		int branchId = Integer.parseInt(request.getParameter("branchId"));
		int positionId = Integer.parseInt(request.getParameter("positionId"));
		String confirmPassword = request.getParameter("confirmPassword");

		if (StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
		}else {
			if (!loginId.matches("[0-9a-zA-Z]+")) {//+によって一回以上の繰り返しを表すことが出来る
				messages.add("ログインIDに不正な文字が含まれています");
			}

			if (loginId.length() < 6 || 20 < loginId.length() ) {
				messages.add("ログインIDが6文字以上20文字以下ではありません");
			}

			if (!isUniqued(loginId)) {
				messages.add("このログインIDは既に使われています。");
			}
		}

		if (!(password.equals(confirmPassword))) {
			messages.add("入力したパスワードが一致しません。");
		}

		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		}else {
			if (!password.matches("[-_@+*;:#$%&A-Za-z0-9]+") ) {//[_A-Za-z0-9]は\wで代用可
				messages.add("パスワードは半角文字で入力してください");
			}
			if (StringUtils.isEmpty(confirmPassword) == true) {
				messages.add("確認用のパスワードが入力されていません。");
			}
		}
		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}
		if (10 < name.length() ) {
			messages.add("名前は10文字以下で入力してください");
		}
		if (branchId == 0) {
			messages.add("支店を選択してください");
		}
		if (positionId == 0) {
			messages.add("部署・役職を選択してください");
		}
		if ( branchId == 1 ) {//本社
			if (positionId == 3 || positionId == 4){
				messages.add("支店と部署・役職の組み合わせが不正です");
			}
		}
		if ( branchId == 2 || branchId == 3 || branchId == 4 ) {//支店
			if (positionId == 1 || positionId == 2) {
				messages.add("支店と部署・役職の組み合わせが不正です");
			}
		}
		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isUniqued(String loginId) {
		User isUniqued = new UserService().isUniqued(loginId);
		if (isUniqued == null) {
			return true;
		}else {
			return false;
		}
	}

}
