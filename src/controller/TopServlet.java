package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.PostUser;
import beans.UserComment;
import service.CommentService;
import service.PostService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		String date1 = request.getParameter("date1");
		String date2 = request.getParameter("date2");
		String argDate1 = null;
		String argDate2 = null;
		String category = request.getParameter("selectCategory");

		if (date1 == null || date1.isEmpty()) {
			argDate1 = "2018-08-01 00:00:00";
		}else {
			argDate1 = date1 +" 00:00:00";
		}
		if (date2 == null || date2.isEmpty()) {
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			argDate2 =  sdf.format(cal.getTime()) + " 23:59:59";
		} else {
			argDate2 = date2 +" 23:59:59";
		}

		List<PostUser> posts = new PostService().getPostUser(argDate1, argDate2, category);
		List<UserComment> comments = new CommentService().getComment();

		request.setAttribute("posts", posts);
		request.setAttribute("comments", comments);
		request.setAttribute("date1", date1);
		request.setAttribute("date2", date2);
		request.setAttribute("category", category);
		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}

}