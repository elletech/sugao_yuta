<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板システム</title>
</head>
<script type="text/javascript">
function commentDeleteCheck(){
	if(window.confirm("このコメントを削除してよろしいでしょうか？")){
		return true; // 「OK」時は送信を実行
	}
	else{//「キャンセル」時の処理
		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止
	}
}
</script>
<body>
	<jsp:include page="header.jsp"/>
	<jsp:include page="errorMessages.jsp"/>
	<div class="main-contents">
		<div class ="search">
		<div class="search-form-label"><b>検索フォーム</b></div>
		<form id="search-form" action="./" method="get">
			<div class="date-area">
				<label class="date_first"><b><i class="fa fa-calendar-o" aria-hidden="true"></i>日付</b></label>
				<input type="date" name="date1" value="${date1}">
				<label class="date_last">～</label>
				<input type="date" name="date2" value="${date2}"><br>
			</div>

			<div class="category-area">
				<label class="category"><b>カテゴリー</b></label>
				<input class="form-control" type="text" name="selectCategory" value="${category}" placeholder="キーワードを入力してください">
			</div>
			<button class="search-button" type="submit" ><i class="fa fa-search search-icon"></i></button>
		</form>
		</div>

		<div class="posts">
			<c:forEach items="${posts}" var="post">
				<div class="post">
					<div class="post-content-body">
						<label class="user_id"><b>投稿者:</b></label>
						<span class="user_id"><c:out value="${post.userName}" /></span><br>
						<label class="subject"><b>件名:</b></label>
						<span class="subject"><c:out value="${post.subject}" /></span><br>
						<label class="body"><b>本文:</b></label>
						<pre class="body"><c:out value="${post.body}" /></pre><br>
						<label class="category"><b>カテゴリー:</b></label>
						<span class="category"><c:out value="${post.category}" /></span><br>

						<div class="date">
						<label class="created_at" ><b><i class="fa fa-calendar-o" aria-hidden="true"></i>投稿日時:</b></label>
							<fmt:formatDate value="${post.createdAt}"
								pattern="yyyy/MM/dd HH:mm:ss" />
						</div>
						<!-- 投稿の削除 -->
						<form action="./deletePost" method="post">
							<input type="hidden" name="postId" value="${post.id}" >
							<c:if test="${post.userId == loginUser.id}">
							<input type="submit" value="投稿を削除" class="square_btn post_delete_btn">
							</c:if>
						</form>
					</div>
					<!-- コメントの表示 -->
					<div class="comments">
						<label><b><i class="fa fa-comment" aria-hidden="true"></i>コメント</b></label>
						<div class="comment">
						<c:forEach items="${comments}" var="comment">
							<c:if test="${post.id == comment.postId}">
								<form action="./deleteComment" method="post" onSubmit="return commentDeleteCheck()">
									<div class="comment-contents">
										<label><b>コメントした人：</b><c:out value="${comment.name}" /></label>
										<pre><c:out value="${comment.body}" /></pre>
										<div class="date">
											<label class="created_at" ><b><i class="fa fa-calendar-o" aria-hidden="true"></i>コメント日時:</b></label>
											<fmt:formatDate value="${comment.createdAt}"
												pattern="yyyy/MM/dd HH:mm:ss" />
										</div>
										<input type="hidden" name="commentId" value="${comment.id}" >
									</div>
									<c:if test="${comment.userId == loginUser.id}">
											<input type="submit" value="コメントを削除" class="square_btn comment_delete_btn">
									</c:if>
								</form>
							</c:if>
						</c:forEach>
						</div>
					</div>

					<!-- 新しくコメントを投稿する -->
					<div class="newComment">
						<div class="form-area">
						<form action="./newComment" method="post">
							<input type="hidden" name="postId" value="${post.id}" >
							<textarea name="commentBody" cols="100" rows="5" class="tweet-box" placeholder="コメント(500文字以下)"></textarea><br />
							<input type="submit" value="コメントを投稿する" class="square_btn comment_create_btn">
						</form>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</body>
</html>
