<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理画面</title>
</head>
<script type="text/javascript">
function deleteCheck(){
	if(window.confirm("このユーザーを停止してよろしいでしょうか？")){
		return true; // 「OK」時は送信を実行
	}
	else{//「キャンセル」時の処理
		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止
	}
}

function restoreCheck(){
	if(window.confirm("このユーザーを復活しますか？")){ // 確認ダイアログを表示
		return true; //「OK」時は送信を実行
	}
	else{ //「キャンセル」時の処理
		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止
	}
}
</script>
<body>
	<jsp:include page="header.jsp"/>
	<div class="main-contents">
		<jsp:include page="errorMessages.jsp"/>
		<h2>ユーザー管理</h2>
		<table>
			<tr>
				<th>ユーザー名</th>
				<th>ログインID</th>
				<th>支店名</th>
				<th>部署・役職</th>
				<th>編集</th>
				<th>停止・復活</th>
			</tr>
			<c:forEach items="${users}" var="user">
				<tr>
					<td><c:out value="${user.name}" /></td>
					<td><c:out value="${user.loginId}" /></td>
					<td><c:out value="${user.branchName}" /></td>
					<td><c:out value="${user.positionName}" /></td>
					<td>
						<form action="./userSetting">
							<input type="hidden" name="id" value="${user.id}">
							<input type="submit" value="編集" style="width: 100px" class="square_btn user_edit_button">
						</form>
					</td>
					<td>
						<c:if test="${user.id != loginUser.id}">
							<c:if test="${user.isDeleted == 0}">
								<form action="./userDelete" method="post" onSubmit="return deleteCheck()">
									<input type="hidden" name="id" value="${user.id}">
									<input type="submit" value="停止" style="width: 100px" class="square_btn user_delete_button">
								</form>
							</c:if>
							<c:if test="${user.isDeleted == 1}">
								<form action="./userRestore" method="post" onSubmit="return restoreCheck()">
									<input type="hidden" name="id" value="${user.id}">
									<input type="submit" value="復活" style="width: 100px" class="square_btn user_restore_button">
								</form>
							</c:if>
						</c:if>

						<c:if test="${user.id == loginUser.id}">
							ログイン中
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</table>
		</div>
</body>
</html>