<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ログイン</title>
</head>
<body>
<jsp:include page="header.jsp"/>
<jsp:include page="errorMessages.jsp"/>
<div class="main-contents">
	<div class="login">
	<form action="./login" method="post" class="login-form">
		<h1 class="login-logo" style="color: white;">Login</h1>
			<div  class="input-group">
				<span class="input-group-addon">
				<input  name="loginId" type="text" class="form-control" value="${loginId}" placeholder="Loginid">
				</span>
			</div>
			<div  class="input-group">
				<span class="input-group-addon">
				<input name="password" type="password" class="form-control" placeholder="Password">
				</span>
			</div>
			<button class="login-button" type="submit" class="float">Login</button>
		</form>
	</div>
</div>
</body>
</html>