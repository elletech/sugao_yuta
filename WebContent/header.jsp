<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<head>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:600" rel="stylesheet">
</head>
<!-- 部分テンプレート（ヘッダーとエラーメッセージ） -->
<header class="site-header clearfix">
	<h1 class="site-logo"><a href="./" id="logo-decoration">ELLETECH</a></h1>
	<nav class="gnav">
	<ul class="gnav__menu">
		<c:if test="${ not empty loginUser }">
			<div class="login_user_name"><c:out value="${loginUser.name}" />でログイン中</div>
				<li class="gnav__menu__item"><a href="./newPost"><i class="fa fa-pencil-square" aria-hidden="true"></i>新規投稿</a></li>
			<c:if test="${ loginUser.positionId == 1 && loginUser.branchId == 1 }">
				<li class="gnav__menu__item"><a href="./userManager"><i class="fa fa-users" aria-hidden="true"></i>ユーザー管理</a></li>
				<li class="gnav__menu__item"><a href="./createUser"><i class="fa fa-user" aria-hidden="true"></i>新規ユーザー作成</a></li>
			</c:if>
			<li class="gnav__menu__item"><a href="./logout"><i class="fa fa-sign-out" aria-hidden="true"></i>ログアウト</a></li>
		</c:if>
	</ul>
    </nav>
</header>
