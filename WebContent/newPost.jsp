<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板システム</title>
</head>
<body>
	<jsp:include page="header.jsp"/>
	<jsp:include page="errorMessages.jsp"/>
	<div class="main-contents">
		<h2>新規投稿</h2>
		<div class="new-post-form">
			<div class="form-area">
				<form action="./newPost" method="post">
					<label for="subject">件名（30文字以下）</label><br />
					<input name="subject" id="subject" value="${post.subject}"/><br />
					<label for="body">本文（1000文字以下）</label><br />
					<textarea name="body" cols="100" rows="10">${post.body}</textarea><br />
					<label for="category">カテゴリー（10文字以下）</label><br />
					<input name="category" id="category" value="${post.category}"/><br />
					<input class="square_btn new-user-button" type="submit" value="投稿する" style="margin-top: 10px;">
				</form>
			</div>
		</div>
	</div>
</body>
</html>