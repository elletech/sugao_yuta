<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザーの編集</title>
</head>
<body>
	<jsp:include page="header.jsp"/>
		<jsp:include page="errorMessages.jsp"/>
	<div class="main-contents">
			<h2 style="color: #2a3644;">ユーザー（<c:out value="${editUser.name}" />）の編集</h2>
			<div class="user-edit-form">
			<form action="./userSetting" method="post">
				<input name="id" value="${editUser.id}" id="id" type="hidden" />
				<label for="loginId">ログインID（半角英数字で6文字以上20文字以下）</label><br />
				<input name="loginId" value="${editUser.loginId}" /><br />
				<label for="name">名称（10文字以下）</label><br />
				<input name="name" value="${editUser.name}" id="name" /><br />
				<label for="password">パスワード（6文字以上20文字以下）</label><br />
				<input name="password" type="password" id="password" /> <br />
				<label for="password">パスワード(確認)</label><br />
				<input name="confirmPassword" type="password" id="confirmPassword" /> <br />

			<c:if test="${editUser.id == loginUser.id}">
				<label for="branchId">支店</label>
				<c:forEach items="${branches}" var="branch">
					<c:if test="${loginUser.branchId == branch.id}">
						<div><c:out value="${branch.name}" /></div><br />
						<input name="branchId" value="${branch.id}" type="hidden" />
					</c:if>
				</c:forEach>
				<label for="positionId">部署・役職</label><br />
				<c:forEach items="${positions}" var="position">
					<c:if test="${loginUser.positionId == position.id}">
						<div><c:out value="${position.name}" /></div><br />
						<input name="positionId" value="${position.id}" type="hidden" />
					</c:if>
				</c:forEach>
			</c:if>

			<c:if test="${editUser.id != loginUser.id}">
				<label for="branchId">支店</label><br />
				<select class="form-control" name="branchId" >
					<c:forEach items="${branches}" var="branch">
					<c:if test="${editUser.branchId == branch.id}">
						<option selected label="" value="${branch.id}"><c:out value="${branch.name}" /></option>
					</c:if>

					<c:if test="${editUser.branchId != branch.id}">
						<option label="" value="${branch.id}"><c:out value="${branch.name}" /></option>
					</c:if>
					</c:forEach>
				</select><br />

				<label for="positionId">部署・役職</label><br />
				<select class="form-control" name="positionId">
					<c:forEach items="${positions}" var="position">
						<c:if test="${editUser.positionId == position.id}">
							<option selected label="" value="${position.id}"><c:out value="${position.name}" /></option>
						</c:if>
						<c:if test="${editUser.positionId != position.id}">
							<option label="" value="${position.id}"><c:out value="${position.name}" /></option>
						</c:if>
					</c:forEach>
				</select>
			</c:if>
			<br /> <input class="square_btn edit-user-button" type="submit" value="編集" /> <br />
		</form>
		</div>
	</div>
</body>
</html>