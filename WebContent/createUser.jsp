<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
</head>
<body>
	<jsp:include page="header.jsp"/>
	<jsp:include page="errorMessages.jsp"/>
	<div class="main-contents">
		<h2>新規ユーザー登録</h2>
		<div class="user-new-form">
			<form action="./createUser" method="post">
				<label for="loginId">ログインID（半角英数字で6文字以上20文字以下）</label><br />
				<input name="loginId" id="loginId" value="${createUser.loginId}"/><br />
				<label for="password">パスワード（半角文字で6文字以上20文字以下）</label><br />
				<input name="password" type="password" id="password"  value="${createUser.password}"/> <br />
				<label for="confirmPassword">パスワード（確認）</label><br />
				<input name="confirmPassword" type="password" id="confirmPassword"/> <br />
				<label for="name">名称(10文字以下)</label><br />
				<input name="name" id="name"  value="${createUser.name}"/> <br />

				<label for="branchId">支店</label><br />
				<select class="form-control" name="branchId" >
					<c:forEach items="${branches}" var="branch">
					<c:if test="${branch.id == createUser.branchId}">
						<option selected label="" value="${branch.id}"><c:out value="${branch.name}" /></option>
					</c:if>

					<c:if test="${branch.id != createUser.branchId}">
						<option label="" value="${branch.id}"><c:out value="${branch.name}" /></option>
					</c:if>
					</c:forEach>
				</select><br />

				<label for="positionId">部署・役職</label><br />
					<select class="form-control" name="positionId">
					<c:forEach items="${positions}" var="position">
						<c:if test="${position.id == createUser.positionId}">
							<option selected label="" value="${position.id}"><c:out value="${position.name}" /></option>
						</c:if>

						<c:if test="${position.id != createUser.positionId}">
							<option label="" value="${position.id}"><c:out value="${position.name}" /></option>
						</c:if>
					</c:forEach>
					</select><br />

				<br /> <input class="square_btn create-user-button" type="submit" value="登録" /> <br />
			</form>
		</div>
	</div>
</body>
</html>